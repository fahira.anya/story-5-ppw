from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests

# Create your views here.
def book_search(request):
    return render(request, "book_search.html")

def book_data(request):
    book = request.GET['book']
    url_src = "https://www.googleapis.com/books/v1/volumes?q=" + book
    result = requests.get(url_src)
    data = result.json()
    return JsonResponse(data, safe=False)