from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.http import HttpRequest
from .views import book_search, book_data
# Create your tests here.

class BookSearchTest(TestCase):

    def test_url_book_search(self):
        response = Client().get("/book-search/")
        self.assertEquals(200, response.status_code)

    def test_function_used_book_search(self):
        found = resolve("/book-search/")
        self.assertEquals(found.func, book_search)

    def test_template_book_search(self):
        response = Client().get("/book-search/")
        self.assertTemplateUsed(response, 'book_search.html')
    
    def test_url_book_data(self):
        response = Client().get('/data?book=frozen')
        self.assertEquals(200, response.status_code)
    
    def test_function_used_book_data(self):
        found = resolve("/data")
        self.assertEquals(found.func, book_data)