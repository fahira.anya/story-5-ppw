from django.test import TestCase, Client
from .views import accordion
from django.urls import resolve
from django.http import HttpRequest
# Create your tests here.
class TestStory7(TestCase):
    def test_url_story7(self):
        response = Client().get("/story-7/")
        self.assertEquals(200, response.status_code)

    def test_template_story7(self):
        response = Client().get('/story-7/')
        self.assertTemplateUsed(response, "story7.html")
    
    def test_function_used_story7(self):
        found = resolve('/story-7/')
        self.assertEquals(found.func, accordion)
    
    def test_content_story7(self):
        request = HttpRequest()
        response = accordion(request)

        isi_html = response.content.decode('utf8')

        self.assertIn('Current Activity', isi_html)
        self.assertIn('Experience', isi_html)
        self.assertIn('Achievement', isi_html)
        self.assertIn('Skills', isi_html)