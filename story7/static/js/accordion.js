var acc = document.getElementsByClassName("accordion-header");

for (var i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}

$('button').click(
  function(e) {
      e.preventDefault();
      var parent = $(this).closest('.accordion-wrap');
      e.stopPropagation();
      if ($(this).hasClass('up')) {
          parent.insertBefore(parent.prev('div'));
      }
      else if ($(this).hasClass('down')) {
          parent.insertAfter(parent.next('div'));
      }
  });