from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import TemplateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', TemplateView.as_view(template_name="start.html"), name='start'),
    path('', include('homepage.urls')),
    path('', include('story1.urls')),
    path('', include('jadwal.urls')),
    path('',include('activity.urls')),
    path('',include('story7.urls')),
    path('',include('story8.urls')),
    path('accounts/',include('story9.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
]
