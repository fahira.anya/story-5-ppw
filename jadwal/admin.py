from django.contrib import admin
from . import models
# Register your models here.
@admin.register(models.AddSchedule)
class AddScheduleAdmin(admin.ModelAdmin):
    list_display = ('sub', 'room','lecturer', 'sks', 'smt','desc')
    search_fields = ('sub', 'room','lecturer', 'sks', 'smt')