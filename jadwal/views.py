from django.shortcuts import render, redirect
from .forms import SchedForm
from .models import AddSchedule

# Create your views here.
def add_sched(request):
    if request.method == "POST":
        form = SchedForm(request.POST)
        if form.is_valid():
            sched = form.save()
            sched.save()
            return redirect('jadwal:add-schedule')
    else:
        form = SchedForm()
    
    return render(request, 'tambah.html', {'form':form})

def delete_sched(request, id):
    AddSchedule.objects.get(id=id).delete()
    return redirect("jadwal:my-schedule")

def schedule(request):
    sched = AddSchedule.objects.order_by("sub")
    return render(request, 'schedule.html', {"sched":sched})

def detail(request,id):
    detail = AddSchedule.objects.get(id=id)
    return render(request,'detail.html',{"detail":detail})