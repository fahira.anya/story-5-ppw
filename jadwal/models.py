from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
# Create your models here.
gsl1 = "Gasal 2019/2020"
gnp1 = "Genap 2019/2020"
gsl2 = "Gasal 2020/2021"
gnp2 = "Genap 2020/2021"
SMT_CHOICES = (
    (gsl1,"Gasal 2019/2020"),
    (gnp1,"Genap 2019/2020"),
    (gsl2,"Gasal 2020/2021"),
    (gnp2,"Genap 2020/2021"),)

class AddSchedule(models.Model):
    sub = models.CharField("Subject",max_length=50, blank=True)

    room = models.CharField("Room",default="online class",max_length=50,blank=True)

    lecturer = models.CharField("Lecturer",max_length=50,blank=True)

    sks = models.IntegerField("Credits", default=1, validators= [MinValueValidator(1), MaxValueValidator(24)])

    smt = models.CharField("Semester",max_length=50, blank=True, choices= SMT_CHOICES, default=gsl2)

    desc = models.TextField("Description",blank=True)

    def __str__(self):
        return f'{self.jadwal}'
    