from django import forms
from .models import AddSchedule
from .models import SMT_CHOICES

class SchedForm(forms.ModelForm):
   class Meta:
       model = AddSchedule
       fields = ['sub', 'room','lecturer', 'sks', 'smt','desc']
       widgets = {
           'sub':forms.TextInput(attrs={'class':'form-control'}),
           'room':forms.TextInput(attrs={'class':'form-control'}),
           'lecturer':forms.TextInput(attrs={'class':"form-control"}),
           'sks':forms.NumberInput(attrs={'class':'form-control','minlength':1, 'maxlength': 6, 'required':True, 'type':'number'}),
           'smt':forms.Select(choices=SMT_CHOICES, attrs={'class':'form-control'}),
           'desc':forms.TextInput(attrs={'class':'form-control'})
       }