from django.urls import path
from . import views

app_name = 'jadwal'

urlpatterns = [
    path('add-schedule/', views.add_sched, name='add-schedule'),
    path('my-schedule/', views.schedule, name="my-schedule"),
    path('delete-schedule/<id>', views.delete_sched, name="delete-sched"),
    path("detail/<id>", views.detail, name="detail")
]  