# Generated by Django 3.1.2 on 2020-10-16 04:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jadwal', '0004_auto_20201015_1741'),
    ]

    operations = [
        migrations.AlterField(
            model_name='addschedule',
            name='room',
            field=models.CharField(blank=True, default='online class', max_length=50, verbose_name='Room'),
        ),
    ]
