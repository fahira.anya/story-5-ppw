from django.urls import path, include
from .views import SignUpView


app_name = 'story9'

urlpatterns = [
    path('signup/', SignUpView.as_view(), name="signup"),
]