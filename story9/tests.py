from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
# Create your tests here.
class TestSignUp(TestCase):
    def test_url_signup(self):
        response = Client().get("/accounts/signup/")
        self.assertEquals(response.status_code, 200)
    
    def test_template_signup(self):
        response = Client().get("/accounts/signup/")
        self.assertTemplateUsed(response, "registration/signup.html")
    
    def test_content_signup(self):
        response = Client().get('/accounts/signup/')
        isi_html = response.content.decode('utf8')

        self.assertIn('Sign Up', isi_html)
        self.assertIn('Username',isi_html)
        self.assertIn('Password',isi_html)
        self.assertIn('Login',isi_html)
        
    
    def setUp(self) -> None:
        self.username = "test"
        self.password = "test1234"
    
    def test_views_signup(self):
        response = self.client.get(reverse('login'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, template_name='registration/login.html')

class TestUserCreationForm(TestCase):
    def test_usercreation_form(self):
        data = {
            'username': 'test',
            'password1': 'test1234',
            'password2': 'test1234',
        }
        form = UserCreationForm(data)
        self.assertFalse(form.is_valid())