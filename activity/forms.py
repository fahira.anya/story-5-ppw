from django import forms
from .models import AddActivity, AddParticipant
class ActivityForm(forms.ModelForm):
    class Meta:
        model = AddActivity
        fields = '__all__'
        widgets = {'activity':forms.TextInput(attrs={'class':'form-control'})}

class ParticipantForm(forms.ModelForm):
    class Meta:
        model = AddParticipant
        fields = '__all__'
        widgets = {
            'activity':forms.Select(attrs={'class':'form-control'}), 
            'name':forms.TextInput(attrs={'class': 'form-control'})}

