from django.db import models

# Create your models here.
class AddActivity(models.Model):
    activity = models.CharField("Activity", max_length=50, blank=False, null=True)
    
    def __str__(self):
        return self.activity

class AddParticipant(models.Model):
    activity = models.ForeignKey(AddActivity, on_delete = models.CASCADE)
    name = models.CharField("Name", max_length=50, blank=False, null= True)

    def __str__(self):
        return self.name