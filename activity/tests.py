from django.test import TestCase, Client, tag, LiveServerTestCase
from .models import AddActivity, AddParticipant
from .views import add_activity, my_activity
from .forms import ActivityForm, ParticipantForm
from selenium import webdriver
from django.urls import reverse, resolve
# Create your tests here.
class TestAddActivity(TestCase):

    def test_url_add_activity(self):
        response = Client().get('/add-activity/')
        self.assertEquals(200, response.status_code)

    def test_template_add_activity(self):
        response = Client().get('/add-activity/')
        self.assertTemplateUsed(response, "add_activity.html")
    
    def test_function_used_add_activity(self):
        found = resolve('/add-activity/')
        self.assertEqual(found.func, add_activity)

    def test_view_add_activity(self):
        response = Client().get('/add-activity/')
        isi_html = response.content.decode('utf8')

        self.assertIn('Add Activity', isi_html)
        self.assertIn('My Activity', isi_html)
        self.assertIn('<form method="POST">', isi_html)
        self.assertIn('<input type="submit" value="Add" class= "btn click font-weight-bold" style="background-color: #FFB39B; color: black; float: right;">', isi_html)

    def test_form_validation(self):
        new_act = AddActivity.objects.create(activity = "ngestory")
        form = ActivityForm(data = {'activity':new_act})
        self.assertTrue(form.is_valid())

    def test_model_add_activity(self):
        AddActivity.objects.create(activity="MAU NONTON")
        data_count = AddActivity.objects.all().count()
        self.assertEquals(data_count, 1)
    
    def test_model_name(self):
        AddActivity.objects.create(activity='Test')
        kegiatan = AddActivity.objects.get(activity='Test')
        self.assertEqual(str(kegiatan),'Test')


class TestAddParticipant(TestCase):

    def test_url_my_activity(self):
        response = Client().get('/my-activity/')
        self.assertEquals(200, response.status_code) 

    def test_template_my_activity(self):
        response = Client().get('/my-activity/')
        self.assertTemplateUsed(response, "my_activity.html")
    
    def test_function_used_my_activity(self):
        found = resolve('/my-activity/')
        self.assertEqual(found.func, my_activity)

    def test_view_my_activity(self):
        response = Client().get('/my-activity/')
        isi_html = response.content.decode('utf8')

        self.assertIn('Add Activity', isi_html)
        self.assertIn('My Activity', isi_html)
        self.assertIn('<form method="POST">', isi_html)
        self.assertIn('<input type="submit" value="Register" class= "btn click font-weight-bold" style="background-color: #FFB39B; float: right;', isi_html)

    def test_form_validation2(self):
        new_act = AddActivity.objects.create(activity = "jalan kaki")
        form = ParticipantForm(data = {'activity':new_act, 'name':'anya'})
        self.assertTrue(form.is_valid())

    def test_model_my_activity(self):
        activity_inp = AddActivity.objects.create(activity="bobok")
        AddParticipant.objects.create(activity=activity_inp, name="jimin")
        data_count = AddParticipant.objects.all().count()
        self.assertEquals(data_count,1)

@tag('functional')
class IndexFunctionalTest(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        sdowner().setUpClass()
        # Change to another webdriver if desired (and downdate CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        sdowner().tearDownClass()
