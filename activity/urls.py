from django.urls import path
from . import views

app_name = 'activity'

urlpatterns = [
    path('add-activity/', views.add_activity, name='add-activity'),
    path('my-activity/', views.my_activity, name='my-activity'),
]