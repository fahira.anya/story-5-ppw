from django.shortcuts import render
from django.http import HttpResponse
from .models import AddActivity, AddParticipant
from .forms import ActivityForm, ParticipantForm
# Create your views here.
def add_activity(request):
    activity_data = AddActivity.objects.all()
    form = ActivityForm()
    if request.method == 'POST':
        forminput = ActivityForm(request.POST)
        if forminput.is_valid():
            forminput.save()
            return render(request, 'add_activity.html', {'form':form, 'status': 'success', 'data' : activity_data})
        else:
            return render(request, 'add_activity.html', {'form':form, 'status': 'failed', 'data' : activity_data})
            
    else:
        return render(request, 'add_activity.html', {'form':form, 'data':activity_data})

def my_activity(request):
    form = ParticipantForm()
    activity_data = AddActivity.objects.all()
    participant_data = AddParticipant.objects.all()
    if request.method == 'POST':
        forminput = ParticipantForm(request.POST)
        if forminput.is_valid():
            forminput.save()
            return render(request, 'my_activity.html', {'form':form, 'status': 'success', 'participant_data' : participant_data, 'activity_data': activity_data})
        else:
            participant_data = AddParticipant.objects.all()
            return render(request, 'my_activity.html', {'form':form, 'status': 'failed', 'participant_data' : participant_data, 'activity_data': activity_data})
    else:
        return render(request, 'my_activity.html', {'form':form, 'status': 'failed', 'participant_data' : participant_data, 'activity_data': activity_data})
